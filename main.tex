\documentclass[10pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\usepackage{float}
\usepackage{mathtools}
\usepackage[underline=true]{pgf-umlsd}
\usepackage{tikz}
\usetikzlibrary{calc}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{shapes.arrows}
\usepackage{array}

\newcommand{\fun}{f: V \rightarrow O}
\newcommand{\mvfun}{M_{mv_f^{t'}}}

\renewcommand{\mess}[4][0]
 {\stepcounter{seqlevel}
  \path
  (#2)+(0,-\theseqlevel*\unitfactor-0.7*\unitfactor) node (mess from) {};
  \addtocounter{seqlevel}{#1}
  \path
  (#4)+(0,-\theseqlevel*\unitfactor-0.7*\unitfactor) node (mess to) {};
  \draw[->,>=angle 60] (mess from) -- (mess to) node[midway, above]
  {#3};

  \node (\detokenize{#3} from) at (mess from) {};
  \node (\detokenize{#3} to) at (mess to) {};
}

\newcommand{\nomess}[3]{%
  \begin{scope}[draw=white]
     \mess{#1}{#2}{#3}
  \end{scope}
}

\title{Secure transmition and evaluation of functions over sensors data in MQTT}

\begin{document}

\maketitle

\section{Protocol}

\noindent Indexes:
\begin{itemize}
    \item $i \in \{0, \dots, n-1\}$: Refers to a publisher.
    \item $j \in \{0, \dots, m-1\}$: Refers to a subscriber.
    \item $t$: Publishing round of a given publisher.
    \item $t'$: Subscription round of a given subscription function.
    \item $k$: Refers to the $k$th bit in a value.
\end{itemize}

\noindent Participants:
\begin{itemize}
    \item $P_i$: The $n$ publishers.
    \item $S_j$: The $m$ subscribers.
    \item $B$: Broker.
    \item $T$: Trusted Party.
\end{itemize}

\noindent Long term secrets:
\begin{itemize}
    \item $pk_B$, $sk_B$: Broker's asymmetric key pair.
    \item $pk_T$, $sk_T$: Trusted Party's asymmetric key pair.
    \item $pk_{P_i}$, $sk_{P_i}$: Publisher $P_i$'s asymmetric key pair.
    \item $pk_{S_j}$, $sk_{S_j}$: Subscriber $S_j$'s asymmetric key pair.
\end{itemize}

\noindent Single round publisher values:
\begin{itemize}
    \item $v_i^t$: Value from publisher $P_i$ at round $t$.
    \item $v_i^{t,k}$: $k$th bit of the value from publisher $P_i$ at round $t$.
    \item $X_0^{i,t,k}$: Label corresponding to 0 for the $k$th bit of $P_i$'s
        value at round $t$.
    \item $X_1^{i,t,k}$: Label corresponding to 1 for the $k$th bit of $P_i$'s
        value at round $t$.
    \item $\mathcal{X}^{i,t} := \left\{ X_0^{i,t,k}, X_1^{i,t,k}: \forall k \in
            \{ 0, \dots, b \} \right\}$: Input value labels from $P_i$'s at
            round $t$.
    \item $\mathcal{X}^{i,t}_{v_i^t} := \left\{ X_{v_i^{t,k}}^{i,t,k}: \forall k
        \in \{ 0, \dots, b \} \right\}$: Mapped Input value from $P_i$'s at
        round $t$.
    \item $T(i)$: Returns the current round performed by the Publisher $P_i$.
\end{itemize}

\noindent Function parameters:
\begin{itemize}
    \item $V$: Function input space.
    \item $O$: Function output space.
    \item $f: V \rightarrow O$: Function that the subscribers are interested in.
\end{itemize}

\noindent Single round subscription values:
\begin{itemize}
    \item $o_f = f(v_0, \cdots, v_{n-1}): v_0, \cdots, v_{n-1} \in V$: Function
        output.
    %\item $o_f^{t',k}$: $k$th bit of the function output at round $t'$.
    %\item $Y_0^{f,t',k}$: Label corresponding to 0 for the $k$th bit of the
    %    function output $o$ at round $t'$.
    %\item $Y_1^{f,t',k}$: Label corresponding to 1 for the $k$th bit of the
    %    function output $o$ at round $t'$.
    \item $mv_f^{t'}$: Masking value at round $t'$.
    \item $T'(i)$: Returns the current round performed for the subscription of
        $f$.
\end{itemize}

\noindent Ratchet keys and derived secrets:
\begin{itemize}
    \item $rk_{P_i}^t$: Publisher $P_i$'s ratchet key at round $t$.
    \item $s_{P_i}^t$: Seed to generate Input value labels $\mathcal{X}^{i,t}$
        from $P_i$ at round $t$.
    \item $rk_f^{t'}$: Subscription to function $f$ ratchet key at round $t'$.
    \item $s_{f}^t$: Seed to generate the Masking value $mv_f^{t'}$ for function
        $f$ at round $t'$.
\end{itemize}

\noindent Parameters:
\begin{itemize}
    \item $l$: Number of bits of the publishers value $v_i$ (function input).
    \item $q$: Number of bits of the function output $o$.
\end{itemize}

\noindent Publisher set up
\begin{enumerate}
    % 1
    \item The administrator registers the Publisher $P_i$'s public key
        $pk_{P_i}$ in the Broker $B$ and in the Trusted Party $T$ for later
        authentication.
    % 2
    \item The Publisher $P_i$ performs a key exchange with the Trusted Party
      $T$, where each party signs its message with its corresponding private
      key ($sk_{P_i}$, $sk_T$).  The key exchange is proxied through the Broker
      $B$, as shown in~\ref{diagram_publisher}.  Every party verifies the
      signature of the received message before proceeding.  The shared secret
      is set to be the ratchet key $rk_{P_i}^0$ used to derive input labels as
      shown in Figure~\ref{diagram_ratchet_pub}.
\end{enumerate}

\input{diagram_publisher}
\input{diagram_ratchet_pub}

\noindent Subscribe set up
\begin{enumerate}
    % 1
    \item The administrator registers the Subscriber $S_j$'s public key
        $pk_{S_j}$ in the Broker $B$ and in the Trusted Party $T$ for later
        authentication.
    % 2
    \item The Subscriber $S_j$ sends an MQTT subscription message to the Broker
      $B$ containing the function $f$ of interest and a fresh public key for a
      key exchange, signed by $S_j$ private key $sk_{S_j}$.  The Broker $B$
      registers this subscription after verifying the signature and forwards it
      to the Trusted Party $T$.  $T$ verifies the signature and checks whether
      a current subscription exists for function $f$ (possibly generated by
      another Subscriber).  If such a subscription doesn't exists, $T$
      generates a new random ratchet key and sets $T'(f)$ to $0$.  $T$ encrypts
      (using a key exchange result of using $\mathtt{A}$ and a fresh private
      key for key exchange $\mathtt{b}$) the current ratchet key for the given
      function $rks_f^{T'(f)}$ along with the current round for subscriptions
      to that function $T'(f)$, which will be $0$ for a new function, or a
      stored value for an already existing function.  Similarly, the ratchet
      key will be a fresh one for a new function or an `evolved' stored one for
      an already existing function.  $T$ sends this information and the key
      echange public key $\mathtt{B}$ along with a signature to $S_j$ through
      $B$.  $B$ will check the signature and store the ratchet key and the
      current round for that function.  $S_j$ will use the ratchet key to
      derive the masking values used to obtain the function output results for
      this subscription in the following rounds, as seen in
      Figure~\ref{diagram_ratchet_sub}. The exchanged messages for this part
      can be seen in Figure~\ref{diagram_subscribe}.
\end{enumerate}

\input{diagram_subscribe}
\input{diagram_ratchet_sub}

\noindent Publication
\begin{enumerate}
    % 1
    \item For every publishing round $t$, the publisher $P_i$ will generate two
        pseudorandom strings for every bit $k$ in value to publish $X_0^{i,t,k}$ and
        $X_1^{i,t,k}$, one corresponding to 0 and the other one corresponding to 1
        respectively, using the ratchet key advanced to round $t$ to generate a
        seed for the cryptographic pseudo-random number generator as shown in
        figure~\ref{diagram_ratchet_pub}.  These will be the input value labels.
        Then the publisher $P_i$ will send the bits of its value $v_i^t$ encoded
        with the labels: $X_{v_i^t}^{i,t}$, along with the current round $t$ and
        a current timestamp $ts$ and a signature of the entire message to the
        Broker $B$, as shown in Figure~\ref{diagram_publish}.
    % 2
    \item The Broker $B$ verifies the signature and stores the received values in
        a table indexable by $i$ and $t$.
\end{enumerate}

\input{diagram_publish}

\noindent Evaluating a function and sending the subscription
\begin{enumerate}
    % 1
    \item Once the Broker $B$ has collected all the required input values from
        the Publishers to compute a specific function, it will request that
        function to be garbled to the Trusted Party $T$.  To do so, $B$ sends a
        definition of the function $f$ along with the latest rounds observed for
        every publisher ${T(0), \dots, T(n-1)}$.
    % 2
    \item The Trusted Party $T$ will garble the circuit generated by composing a
        masking function $M$ using $mv_f^{t'}$ as the mask with the function $f$
        sent by the Broker $B$.  To generate the input labels for the garbled
        circuit, $T$ will advance the ratchet keys from every publisher to the
        required round $T(i)$ which in turn are used to generate a seed to feed
        a cryptographic pseudo-random number generator from which the labels
        themselves will be obtained, as shown in
        Figure~\ref{diagram_ratchet_pub}.  This procedure should generate be
        the same input labels as the ones computed by the publishers.  In a
        similar fashion, the masking value is generated from the subscription
        ratchet key advanced to round $t'$, as shown in
        Figure~\ref{diagram_ratchet_sub}.  The garbled circuit is sent to the
        Broker $B$ along with the required information to decode the circuit
        output (i.e., the circuit output labels).
    % 3
    \item The Broker $B$ now evaluates the received garbled circuit using the
        previously stored encoded input values from each publisher $P_i$ at time
        $T(i)$.  At this step, $B$ is unable to learn the result of the
        function because it is hidden my a pseudo-random mask.  $B$ sends the
        output, along with the function $f$ description, the subscription round
        $t'$ with a signature of the entire message to all the subscribers.
    % 4
    \item A Subscriber $S_j$ that receives the subscription message will first
        verify the signature of the message.  Then it will derive the masking
        value $mv_f^{t'}$ by advancing the subscription ratchet key to round
        $t'$: $rk_f^{t'}$, in order to generate a seed used to feed the
        cryptographic pseudo-random number generator from which the masking
        value is obtained, which should be the same as the one computed by $T$.
        With this, $S_j$ is able to unmask the composed function output and thus
        obtain the desired function $f$ output: $o_f$ for round $t'$.  All the
        steps of this part can be seen in Figure~\ref{diagram_subscription}.
\end{enumerate}

\input{diagram_subscription}

\subsection{Notes}
We are evaluating functions over sensor values over the "sensor" domain.  What
if we want to evaluate functions over the "time" domain?  (For example, over a
set of sensors, the average value over the last 10 values).

The communication between the Broker and the Trusted Party is private and
authenticated, for example by using TLS with client and server authentication.

\subsection{Things to be aware of}

\begin{itemize}
    \item A publisher should not be able to read the values from other
        publishers.  The channel between a publisher and the Broker needs
        confidentiality.
    \item A publisher should not be able to tamper with the values sent by other
        publishers.  The channel between a publisher and the Broker needs
        authentication/integrity.
    \item A subscriber should not be able to read the output value of a function
        he is not subsribed to.  The channel between the Broker and a subscriber
        needs confidentiality.
    \item A subscriber should not be able to tamper with the values sent by
        other publishers.  The channel between the Broker and the subscriber
        needs authentcation/integrity.
    \item A subscriber should not be able to decide an arbitrary function for
        the protocol, or else a subscriber could choose a function that reveals
        every publisher value.
    \item What happens if there is a collusion between B and a subscriber?  B
        would learn the output of the function f, but if B is malicious, can he
        learn more?  What about TP?
    \item Be careful with replay attacks.
\end{itemize}

\subsection{About seeds}

If every publisher uses a different seed (that is synchronized with the server)
then we can have 2-party secure communications and thus only need MAC to provide
authentication, avoiding the need for public key crypto on every publication.

The same happens with the subscribers.

Every subscriber has a different seed.

Good:
\begin{itemize}
    \item No need to use public key crypto to authenticate Broker messages: just
        use MAC.
    \item If a subscriber leaves, the Broker just won't use that subscriber's
        seed to encrypt the message.
\end{itemize}

Bad:
\begin{itemize}
    \item Broker must sent a different message to each subscriber, although the
        plaintext is the same (different MAC for each one).
\end{itemize}

Every subscriber has a the same seed.

Good:
\begin{itemize}
    \item Broker just needs to craft a single message for all the subscribers.
\end{itemize}

Bad:
\begin{itemize}
    \item Every subscriber needs to verify the authentication using public key
        crypto.
    \item Broker needs to communicate with all the subscribers if one leaves in
        order to set up a new seed.
\end{itemize}

\end{document}
